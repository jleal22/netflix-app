import { Component, OnInit } from '@angular/core';
//Model-Interface
import { FilmGallery } from './models/netflix-model';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.scss'],
})
export class GlobalComponent implements OnInit {
  public heroFilmData: FilmGallery;
  public filmData: FilmGallery[];
  constructor() {
    this.heroFilmData = {
      category: 'Los 5 más populares hoy',
      films: [
        {
          title: 'El Juego Del Calamar',
          img: {
            src: 'https://image.tmdb.org/t/p/w342/yvW9VuHiwfAaTNYjALROI8evNIT.jpg',
            alt: 'El Juego Del Calamar',
          },
        },
        {
          title: 'Venom',
          img: {
            src: 'https://image.tmdb.org/t/p/w342/jMBTJQiHAyGlZR05J2sq5coA6ew.jpg',
            alt: 'Venom',
          },
        },
        {
          title: 'F9',
          img: {
            src: 'https://image.tmdb.org/t/p/w342/bOFaAXmWWXC3Rbv4u4uM9ZSzRXP.jpg',
            alt: 'F9',
          },
        },
        {
          title: 'Free Guy',
          img: {
            src: 'https://image.tmdb.org/t/p/w342/suaooqn1Mnv60V19MoGxneMupJs.jpg',
            alt: 'Free Guy',
          },
        },
        {
          title: 'Loki',
          img: {
            src: 'https://image.tmdb.org/t/p/w342/kAHPDqUUciuObEoCgYtHttt6L2Q.jpg',
            alt: 'Loki',
          },
        },
      ],
    };

    this.filmData = [
      {
        category: 'Series',
        films: [
          {
            title: 'Anatomía de Grey',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/9ii38fa8Vqb9dRv97qEvoGbGftE.jpg',
              alt: 'Anatomía de Grey',
            },
          },
          {
            title: 'The Good Doctor',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/ubOaaQjDQ4lWtw1dkXhqsQWTsEY.jpg',
              alt: 'The Good Doctor',
            },
          },
          {
            title: 'Rick y Morty',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/5Yiep9EwcQgLolg013ETBVqHxuD.jpg',
              alt: 'Rick y Morty',
            },
          },
          {
            title: 'Suits',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/vQiryp6LioFxQThywxbC6TuoDjy.jpg',
              alt: 'Suits',
            },
          },
          
        ],
      },
      {
        category: 'Acción y Aventura',
        films: [
          {
            title: 'Asesinos de Elite',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/a3Ep5s2af2Fy8hlWCH3lbLGZdAH.jpg',
              alt: 'Asesinos de Elite',
            },
          },
          {
            title: 'Halloween Kills',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/5zPNs90yGHKdKadN2Bz32Pug8jY.jpg',
              alt: 'Halloween Kills',
            },
          },
          {
            title: 'SEE',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/A6dnHWe8YYcoFBHzP7T6WPP4b6F.jpg',
              alt: 'SEE',
            },
          },
          {
            title: 'La guerra del mañana',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/tzp6VzED2TBc02bkYoYnQa6r2OK.jpg',
              alt: 'La guerra del mañana',
            },
          },
        ],
      },
      {
        category: 'Nuevo en Netflix',
        films: [
          {
            title: 'La Asistenta',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/4brWcSXdH31BZUTtRTHj2BYFe6M.jpg',
              alt: 'La Asistentad',
            },
          },
          {
            title: 'Luca',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/j93NHWoTjdsL4sdz058M3AvKrMy.jpg',
              alt: 'Luca',
            },
          },
          {
            title: 'The Tonight Show',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/if7ECoH4xaYw5S8gomNIEmtwTxP.jpg',
              alt: 'The Tonight Show',
            },
          },
          {
            title: 'Warning',
            img: {
              src: 'https://image.tmdb.org/t/p/w342/elaQvcOaor2leCCrI5PH6hyps9o.jpg',
              alt: 'Warning',
            },
          },
          
        ],
      },
    ];
  }

  ngOnInit(): void {}
}
