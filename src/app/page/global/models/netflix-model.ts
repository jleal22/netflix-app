export interface NetflixModel {
}

export interface BatmanData {
    hero: Hero;
    gallery: Gallery[];
    info: Info;
}

export interface Hero{
    title: string;
    img: Img;
    description: string;
}

export interface Gallery{
    title: string;
    img: Img;
}

export interface Info{
    name: string;
    age: number;
    location: string;
    job: string;
    isFather: boolean;
    isMarried: boolean;
}

export interface Img {
    src: string;
    alt: string;
}

export interface Film {
    title: string;
    img: Img;
}

export interface FilmGallery {
    category: string;
    films: Film[];
}
